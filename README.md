Hasker - Questions & Answers Web Application
=====================

Questions & Answers Web Application

## Requirements ##

* Python 3.7

* Postgresql

* AWS S3 bucket - project uses it for static & media (user generated) files.

## Installation

`git clone https://gitlab.com/ivan812/hasker.git`
`cd hasker`
`make`

Or push project to GitLab and it will be auto-tested and auto-deployed to the Heroku dyno.

For local configuration use `settings_local.py`, see `settings_local.template` as example

## Runing ##

`make run`

`Ctrl+C` - stop the server

Default admin access:

http://127.0.0.1:8000/admin/

login: `admin`
password: `adminadmin`

## Testing ##

`make test`

## REST API ##

Application has REST API: https://127.0.0.1:8000/api/v1/

It has JWT authentication.