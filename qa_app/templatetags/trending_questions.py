from django import template
from hasker.settings import TRENDING_QUESTIONS
from qa_app.models import Question

register = template.Library()


@register.inclusion_tag('includes/trending_questions.html')
def get_trending_questions():
    return {'trending_questions': Question.get_trending()[:TRENDING_QUESTIONS]}
