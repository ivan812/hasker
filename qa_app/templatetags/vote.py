from django import template

register = template.Library()


@register.inclusion_tag('includes/vote_widget.html')
def vote(model_obj):
    if hasattr(model_obj, 'pk') and hasattr(model_obj, 'votes_sum'):
        return {'model': str(type(model_obj).__name__).lower(),
                'pk': model_obj.pk,
                'votes_sum': model_obj.votes_sum}
    raise template.TemplateSyntaxError('Object {} does not have "pk"'.format(model_obj))
