from django.test import TestCase, Client
from qa_app.models import Question, Answer, User, Tag, Vote


class QuestionTestCase(TestCase):

    users = [None, ]

    def setUp(self):
        for i in range(2, 7):
            self.users.append(User.objects.create(pk=i, username='testuser{}'.format(i), password='7UhDZMBV5z'))
        for i in range(1, 6):
            Tag.objects.create(name='название тега1{}'.format(i))

    def test_question_create(self):
        question = Question.objects.create(author=self.users[1],
                                           title='Сколько сейчас времени?',
                                           content='Содержание вопроса')
        self.assertTrue(question)
        self.assertTrue(question.created_at)
        self.assertEqual(question.author, self.users[1])
        self.assertEqual(question.title, 'Сколько сейчас времени?')
        self.assertEqual(question.content, 'Содержание вопроса')

    def test_question_tag_create(self):
        question = Question.objects.create(author=self.users[1], title='1', content='2')
        question.tags.set(Tag.objects.all()[:3])
        self.assertEqual(question.tags.count(), 3)

    def test_answer_create(self):
        question = Question.objects.create(author=self.users[1], title='Вопрос1?')
        Answer.objects.create(author=self.users[1], question=question, content='Ответ1')
        answer = Answer.objects.get(content='Ответ1')
        self.assertTrue(answer.author, self.users[1])
        self.assertEqual(answer.question, question)
        self.assertEqual(answer.content, 'Ответ1')
        self.assertTrue(answer.created_at)

    def test_question_has_answer(self):
        question = Question.objects.create(author=self.users[1], title='Заголовок вопроса?', content='Тело вопроса.')
        question.answers.create(author=self.users[1], content='Ответ1')
        self.assertFalse(question.has_answer)
        question.answers.create(author=self.users[1], content='Ответ2', is_right=True)
        self.assertTrue(question.has_answer)

    def test_question_votes_sum(self):
        question = Question.objects.create(author=self.users[1], title='Заголовок вопроса?', content='Тело вопроса.')
        self.assertEqual(question.votes_sum, 0)
        question.votes.create(value=1, user=User.objects.get(pk=2))
        question.votes.create(value=-1, user=User.objects.get(pk=3))
        question.votes.create(value=1, user=User.objects.get(pk=4))
        question.votes.create(value=1, user=User.objects.get(pk=5))
        question.votes.create(value=1, user=User.objects.get(pk=6))
        self.assertEqual(question.votes_sum, 3)

    def test_answer_votes_sum(self):
        question = Question.objects.create(author=self.users[1], title='Заголовок вопроса?', content='Тело вопроса.')
        answer = question.answers.create(author=self.users[1], content='Ответ')
        self.assertEqual(answer.votes_sum, 0)
        answer.votes.create(value=1, user=User.objects.get(pk=2))
        answer.votes.create(value=-1, user=User.objects.get(pk=3))
        answer.votes.create(value=1, user=User.objects.get(pk=4))
        answer.votes.create(value=1, user=User.objects.get(pk=5))
        answer.votes.create(value=1, user=User.objects.get(pk=6))
        self.assertEqual(answer.votes_sum, 3)

    def test_question_get_last(self):
        question = Question.objects.create(author=self.users[1], title='Заголовок вопроса1?', content='Тело вопроса.')
        question.answers.create(author=self.users[2], content='Ответ1')
        question = Question.objects.create(author=self.users[2], title='Заголовок вопроса2?', content='Тело вопроса.')
        question.answers.create(author=self.users[3], content='Ответ1')
        question.answers.create(author=self.users[2], content='Ответ2')
        question.answers.create(author=self.users[4], content='Ответ3')
        question = Question.objects.create(author=self.users[3], title='Заголовок вопроса3?', content='Тело вопроса.')
        question.answers.create(author=self.users[5], content='Ответ1')
        question.answers.create(author=self.users[3], content='Ответ2')
        question = Question.objects.create(author=self.users[4], title='Заголовок вопроса4?', content='Тело вопроса.')
        question = Question.objects.create(author=self.users[5], title='Заголовок вопроса5?', content='Тело вопроса.')
        question.answers.create(author=self.users[4], content='Ответ1')
        last = Question.get_last()
        self.assertEqual(last[0].title, 'Заголовок вопроса5?')
        self.assertEqual(last[1].title, 'Заголовок вопроса4?')
        self.assertEqual(last[2].title, 'Заголовок вопроса3?')
        self.assertEqual(last[3].title, 'Заголовок вопроса2?')
        self.assertEqual(last[4].title, 'Заголовок вопроса1?')
        self.assertEqual(last[0].count_answers, 1)
        self.assertEqual(last[1].count_answers, 0)
        self.assertEqual(last[2].count_answers, 2)
        self.assertEqual(last[3].count_answers, 3)
        self.assertEqual(last[4].count_answers, 1)

    def test_question_get_hot(self):
        question = Question.objects.create(author=self.users[1], title='Заголовок вопроса1?', content='Тело вопроса.')
        question.answers.create(author=self.users[2], content='Ответ1')
        question = Question.objects.create(author=self.users[2], title='Заголовок вопроса2?', content='Тело вопроса.')
        question.answers.create(author=self.users[3], content='Ответ1')
        question.answers.create(author=self.users[2], content='Ответ2')
        question.answers.create(author=self.users[4], content='Ответ3')
        question = Question.objects.create(author=self.users[3], title='Заголовок вопроса3?', content='Тело вопроса.')
        question.answers.create(author=self.users[5], content='Ответ1')
        question.answers.create(author=self.users[3], content='Ответ2')
        question = Question.objects.create(author=self.users[4], title='Заголовок вопроса4?', content='Тело вопроса.')
        question = Question.objects.create(author=self.users[5], title='Заголовок вопроса5?', content='Тело вопроса.')
        question.answers.create(author=self.users[4], content='Ответ1')
        last = Question.get_hot()
        self.assertEqual(last[0].title, 'Заголовок вопроса2?')
        self.assertEqual(last[1].title, 'Заголовок вопроса3?')
        self.assertEqual(last[2].title, 'Заголовок вопроса5?')
        self.assertEqual(last[3].title, 'Заголовок вопроса1?')
        self.assertEqual(last[4].title, 'Заголовок вопроса4?')
        self.assertEqual(last[0].count_answers, 3)
        self.assertEqual(last[1].count_answers, 2)
        self.assertEqual(last[2].count_answers, 1)
        self.assertEqual(last[3].count_answers, 1)
        self.assertEqual(last[4].count_answers, 0)

    def test_question_get_trending(self):
        question = Question.objects.create(author=self.users[1], title='Заголовок вопроса1?', content='Тело вопроса.')
        question.votes.create(user=self.users[2], value=1)
        question.votes.create(user=self.users[3], value=1)
        question.votes.create(user=self.users[5], value=1)
        question = Question.objects.create(author=self.users[2], title='Заголовок вопроса2?', content='Тело вопроса.')
        question.votes.create(user=self.users[3], value=1)
        question.votes.create(user=self.users[2], value=-1)
        question.votes.create(user=self.users[4], value=1)
        question = Question.objects.create(author=self.users[3], title='Заголовок вопроса3?', content='Тело вопроса.')
        question.votes.create(user=self.users[5], value=-1)
        question.votes.create(user=self.users[3], value=-1)
        question = Question.objects.create(author=self.users[4], title='Заголовок вопроса4?', content='Тело вопроса.')
        question = Question.objects.create(author=self.users[5], title='Заголовок вопроса5?', content='Тело вопроса.')
        question.votes.create(user=self.users[4], value=-1)
        last = Question.get_trending()
        self.assertEqual(last[0].title, 'Заголовок вопроса2?')
        self.assertEqual(last[1].title, 'Заголовок вопроса1?')
        self.assertEqual(last[2].title, 'Заголовок вопроса3?')
        self.assertEqual(last[3].title, 'Заголовок вопроса5?')
        self.assertEqual(last[4].title, 'Заголовок вопроса4?')
        self.assertEqual(last[0].count_votes, 3)
        self.assertEqual(last[1].count_votes, 3)
        self.assertEqual(last[2].count_votes, 2)
        self.assertEqual(last[3].count_votes, 1)
        self.assertEqual(last[4].count_votes, 0)

    def test_question_search(self):
        Question.objects.create(author=self.users[1], title='Заголовок вопроса1: коптер?',
                                content='Тело вопроса. Слова для проверки работы поиска: '
                                        'квадрокоптер, квадро, коптерка.')
        Question.objects.create(author=self.users[1], title='Заголовок вопроса2: дрынолет?',
                                content='Тело вопроса. Слова для проверки работы поиска: '
                                        'мама мыла раму.')
        Question.objects.create(author=self.users[1], title='Заголовок вопроса3: Коптерка?',
                                content='Тело вопроса. Слова для проверки работы поиска: '
                                        'рама мыла маму.')
        Question.objects.create(author=self.users[1], title='Заголовок вопроса4: каптерка?',
                                content='Тело вопроса. Слова для проверки работы поиска: '
                                        'квадратное уравнение.')
        Question.objects.create(author=self.users[1], title='Заголов вопроса5: Ёлка?',
                                content='Тело вопроса. Слова для проверки работы поиска: '
                                        'квадрицепцы и бицепцы')
        self.assertEqual(Question.search('коптер').count(), 2)
        self.assertEqual(Question.search('КОПТЕР').count(), 2)
        self.assertEqual(Question.search('Заголовок').count(), 4)
        self.assertEqual(Question.search('квадро').count(), 1)
        self.assertEqual(Question.search('квадр').count(), 3)
        self.assertEqual(Question.search('цепцы').count(), 1)
        self.assertEqual(Question.search('мама').count(), 1)
        self.assertEqual(Question.search('мам').count(), 2)

    def test_question_get_by_tag(self):
        tag1 = Tag.objects.create(name='тег1')
        tag2 = Tag.objects.create(name='Тег2')
        tag3 = Tag.objects.create(name='ТЕГ3')
        tag4 = Tag.objects.create(name='тег4')
        tag5 = Tag.objects.create(name='тег5')
        tag6 = Tag.objects.create(name='тег6')
        question = Question.objects.create(author=self.users[1], title='Заголовок1', content='Тело вопроса.')
        question.tags.add(tag1, tag2, tag3)
        question = Question.objects.create(author=self.users[1], title='Заголовок2', content='Тело вопроса.')
        question.tags.add(tag1, tag5)
        question = Question.objects.create(author=self.users[1], title='Заголовок3', content='Тело вопроса.')
        question.tags.add(tag4, tag2)
        question = Question.objects.create(author=self.users[1], title='Заголовок4', content='Тело вопроса.')
        question = Question.objects.create(author=self.users[1], title='Заголовок5', content='Тело вопроса.')
        question.tags.add(tag3)
        self.assertEqual(Question.get_by_tag('тег1').count(), 2)
        self.assertEqual(Question.get_by_tag('ТЕГ1').count(), 2)
        self.assertEqual(Question.get_by_tag('тег2').count(), 2)
        self.assertEqual(Question.get_by_tag('тег3').count(), 2)
        self.assertEqual(Question.get_by_tag('тег4').count(), 1)
        self.assertEqual(Question.get_by_tag('тег5').count(), 1)
        self.assertEqual(Question.get_by_tag('тег6').count(), 0)
        self.assertEqual(Question.get_by_tag('тег7').count(), 0)
        self.assertEqual(Question.get_by_tag('тег').count(), 0)

    def test_question_vote(self):
        question = Question.objects.create(author=self.users[1], title='Заголовок1', content='Тело вопроса.')
        Vote.vote(question, self.users[2], 1)
        self.assertEqual(question.votes_sum, 1)
        Vote.vote(question, self.users[3], 1)
        self.assertEqual(question.votes_sum, 2)
        Vote.vote(question, self.users[2], 1)
        self.assertEqual(question.votes_sum, 2)
        Vote.vote(question, self.users[2], -1)
        self.assertEqual(question.votes_sum, 0)
        Vote.vote(question, self.users[4], 1)
        self.assertEqual(question.votes_sum, 1)
        Vote.vote(question, self.users[5], -1)
        self.assertEqual(question.votes_sum, 0)

    def test_answer_vote(self):
        question = Question.objects.create(author=self.users[1], title='Заголовок1', content='Тело вопроса.')
        answer = Answer.objects.create(author=self.users[1], question=question, content='Ответ')
        Vote.vote(answer, self.users[2], 1)
        self.assertEqual(answer.votes_sum, 1)
        Vote.vote(answer, self.users[3], 1)
        self.assertEqual(answer.votes_sum, 2)
        Vote.vote(answer, self.users[2], 1)
        self.assertEqual(answer.votes_sum, 2)
        Vote.vote(answer, self.users[2], -1)
        self.assertEqual(answer.votes_sum, 0)
        Vote.vote(answer, self.users[4], 1)
        self.assertEqual(answer.votes_sum, 1)
        Vote.vote(answer, self.users[5], -1)
        self.assertEqual(answer.votes_sum, 0)
