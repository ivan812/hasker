from django.test import TestCase
from qa_app.models import Tag
from qa_app.forms import QuestionForm


class QuestionFormTestCase(TestCase):

    def setUp(self):
        for i in range(1, 6):
            Tag.objects.create(pk=i, name='Тег{}'.format(i))

    def test_question_form(self):
        form_data = {'title':'Тайтл вопроса', 'content': 'Содержание вопроса', 'tags': []}
        question_form = QuestionForm(data=form_data)
        self.assertTrue(question_form.is_valid())
        form_data = {'title':'Тайтл вопроса', 'content': 'Содержание вопроса', 'tags': [1]}
        question_form = QuestionForm(data=form_data)
        self.assertTrue(question_form.is_valid())
        form_data = {'title':'Тайтл вопроса', 'content': 'Содержание вопроса', 'tags': [1, 2, 3]}
        question_form = QuestionForm(data=form_data)
        self.assertTrue(question_form.is_valid())
        form_data = {'title':'Тайтл вопроса', 'content': 'Содержание вопроса', 'tags': [1, 2, 3, 4]}
        question_form = QuestionForm(data=form_data)
        self.assertFalse(question_form.is_valid())
