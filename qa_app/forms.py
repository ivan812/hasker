from django import forms
from django.core.exceptions import ValidationError

from hasker.settings import MAX_TAGS
from qa_app.models import Question, Answer


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['title', 'content', 'tags']

    def clean_tags(self):
        data = self.cleaned_data['tags']
        if data.count() > MAX_TAGS:
            raise ValidationError('Maximum 3 tags')
        return data


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['content', 'question']
        widgets = {'question': forms.HiddenInput, }


class SearchForm(forms.Form):
    search = forms.CharField()
