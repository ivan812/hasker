from django import views
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.core.mail import send_mail
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from django.http import HttpResponseRedirect, HttpResponseNotFound, HttpResponseForbidden, HttpResponseNotAllowed
from django.views.generic import CreateView, DetailView, ListView
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
import logging

from qa_app.forms import QuestionForm, AnswerForm
from qa_app.models import Question, Answer, Tag, Vote


logger = logging.getLogger('hasker.qa_app')


class IndexListView(ListView):
    model = Question
    template_name = 'index.html'
    context_object_name = 'last_questions'
    paginate_by = 10

    def get_queryset(self):
        if self.request.path == reverse('qa:hot'):
            return Question.get_hot()
        return Question.get_last()


class SearchListView(ListView):
    model = Question
    template_name = 'search.html'
    context_object_name = 'found_questions'
    paginate_by = 20

    def get_queryset(self):
        text = str(self.request.GET.get('q')).strip()
        if text.lower().startswith('tag:'):
            tag_name = text[4:].strip()
            try:
                return Question.get_by_tag(tag_name)
            except ObjectDoesNotExist:
                return Question.objects.none()
        return Question.search(text)


class QuestionCreateView(LoginRequiredMixin, CreateView):
    model = Question
    form_class = QuestionForm
    login_url = 'login'

    template_name = 'question_form.html'

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('qa:question', kwargs={'pk': self.object.pk})


class QuestionCreateAnswerView(CreateView):
    model = Answer
    form_class = AnswerForm
    template_name = 'question.html'

    def send_notification(self):
        email_subject = 'New answer from {}'.format(self.object.author)
        email_message = 'You have a new answer from {} on "{}":\r\n' \
                        '\r\n' \
                        '{}\r\n' \
                        '\r\n' \
                        '{}'.format(self.object.author,
                                    self.object.question.title,
                                    self.object.content,
                                    self.request.build_absolute_uri())
        try:
            send_mail(email_subject,
                      email_message,
                      'ivan@urlix.ru',
                      [self.object.question.author.email],
                      fail_silently=False)
        except Exception as e:
            # for Heroku deployment, change to external service in the future
            logger.debug("We've got an error during send mail via gmail: {}".format(e))

    def form_valid(self, form):
        self.success_url = self.request.path
        form.instance.author = self.request.user
        form_valid_result = super().form_valid(form)
        if form_valid_result and self.object.author.email:
            self.send_notification()
        return form_valid_result

    def get_context_data(self, **kwargs):
        self.initial = {'question': self.kwargs['pk']}
        context = super().get_context_data(**kwargs)
        question = get_object_or_404(Question, pk=self.kwargs['pk'])
        answers = question.get_sorted_answers()
        paginator = Paginator(answers, settings.ANSWERS_PER_PAGE)
        try:
            answers = paginator.get_page(self.request.GET.get('page'))
        except (PageNotAnInteger, KeyError) as e:
            answers = paginator.get_page(1)
        except EmptyPage:
            answers = paginator.get_page(paginator.num_pages)

        context.update({
            'question': question,
            'answers': answers,
            'paginator': paginator,
            'page_obj': answers,
        })
        return context
    #
    # def detail(request, id):
    #     context = dict()
    #     return render(request, 'question.html', context)


class TagDetailView(DetailView):
    model = Tag
    template_name = 'tag.html'


class RightAnswerActionView(LoginRequiredMixin, views.View):
    login_url = 'login'

    def get(self, request, pk):
        try:
            answer = get_object_or_404(Answer, pk=pk)
            if answer.question.author == request.user or request.user.is_superuser:
                # and not answer.question.answers.filter(is_right=True) :
                answer.question.answers.filter(is_right=True).update(is_right=False)
                answer.is_right = True
                answer.save()
                return redirect(answer.question)
            raise PermissionDenied('Incorrect user')
        except PermissionDenied:
            return HttpResponseForbidden('You DO NOT have an access')
        except Exception as e:
            logger.debug('Incorrect request: {}'.format(e))
            return HttpResponseNotAllowed('Incorrect request')


class VoteActionView(LoginRequiredMixin, views.View):
    login_url = 'login'

# TODO: refactor vote design
    def get(self, request, model_entity, pk, value_alias):
        if type(pk) is int \
           and type(model_entity) is str and model_entity in ('question', 'answer') \
           and type(value_alias) is str and value_alias in ('like', 'dislike'):
            try:
                value = 1 if value_alias == 'like' else -1
                if model_entity == 'answer':
                    answer = get_object_or_404(Answer, pk=pk)
                    question = answer.question
                    Vote.vote(answer, request.user, value)
                if model_entity == 'question':
                    question = get_object_or_404(Question, pk=pk)
                    Vote.vote(question, request.user, value)
                return redirect(question)
            except ObjectDoesNotExist as e:
                logger.debug("We've got an error during voting process: {}".format(e))

        return HttpResponseNotFound('Incorrect request')


class MyQuestionsListView(ListView):
    model = Question
    paginate_by = 10
    template_name = 'myquestions.html'

    def get_queryset(self):
        return Question.get_by_user(self.request.user)


class MyAnswersListView(ListView):
    model = Answer
    paginate_by = 10
    template_name = 'myanswers.html'

    def get_queryset(self):
        return Answer.get_by_user(self.request.user)
