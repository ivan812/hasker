from django.apps import AppConfig


class QAAppConfig(AppConfig):
    name = 'qa_app'
