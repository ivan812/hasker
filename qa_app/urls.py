from django.urls import path

from qa_app import views


urlpatterns = [
    path('hot', views.IndexListView.as_view(), name='hot'),
    path('search', views.SearchListView.as_view(), name='search'),
    path('accounts/myquestions/', views.MyQuestionsListView.as_view(), name='myquestions'),
    path('accounts/myanswers/', views.MyAnswersListView.as_view(), name='myanswers'),
    path('question/new', views.QuestionCreateView.as_view(), name='new_question'),
    path('question/<int:pk>', views.QuestionCreateAnswerView.as_view(), name='question'),
    path('tag/<int:pk>', views.TagDetailView.as_view(), name='tag'),
    path('right_answer/<int:pk>', views.RightAnswerActionView.as_view(), name='right_answer'),
    path('vote/<str:model_entity>/<int:pk>/<str:value_alias>', views.VoteActionView.as_view(), name='vote'),
]