from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from rest_framework_simplejwt.settings import api_settings
from qa_app.models import *


class APIJWTClient(APIClient):
    def login(self, **credentials):
        """
        Returns True if login is possible; False if the provided credentials
        are incorrect, or the user is inactive.
        """
        response = self.post(reverse('token_obtain_pair'), credentials, format='json')
        if response.status_code == status.HTTP_200_OK:
            self.credentials(
                # for "rest_framework_jwt" use:
                # HTTP_AUTHORIZATION="{prefix} {token}".format(prefix=api_settings.JWT_AUTH_HEADER_PREFIX,
                #                                              token=response.data['token']))
                HTTP_AUTHORIZATION="{prefix} {token}".format(prefix=api_settings.AUTH_HEADER_TYPES[0],
                                                             token=response.data['access']))
            return True
        else:
            return False


class APIJWTTestCase(APITestCase):
    client_class = APIJWTClient


class ExampleTestCase(APIJWTTestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='admin', password='pass')
        self.question = Question.objects.create(author=self.user, title='Заголовок вопроса1?', content='Тело вопроса.')
        self.question.tags.create(name='Тег1')
        self.question.answers.create(author=self.user, content='Ответ1')
        self.question2 = Question.objects.create(author=self.user, title='Другой вопр для теста поиска', content='содержание')
        self.question2.tags.create(name='Тег2')

    def login(self):
        self.client.login(username='admin', password='pass')

    def test_auth(self):
        user = User.objects.create_user(username='consumer', password='pass')
        user.is_active = False
        user.save()
        is_logged = self.client.login(username='consumer', password='pass')
        self.assertFalse(is_logged)

        user.is_active = True
        user.save()
        is_logged = self.client.login(username='consumer', password='pass')
        self.assertTrue(is_logged)

        is_logged = self.client.login(username='consumer', password='wrong')
        self.assertFalse(is_logged)

    def test_question_access(self):
        url = reverse('question-list')
        response = self.client.get(url)
        self.assertTrue(status.is_client_error(response.status_code))
        self.login()
        response = self.client.get(url)
        self.assertTrue(status.is_success(response.status_code))

    def test_answer_access(self):
        url = reverse('answers', kwargs={'q_pk': 1})
        response = self.client.get(url)
        self.assertTrue(status.is_client_error(response.status_code))
        self.login()
        response = self.client.get(url)
        self.assertTrue(status.is_success(response.status_code))

    def test_search_access(self):
        url = reverse('search', kwargs={'q': 'запрос'})
        response = self.client.get(url)
        self.assertTrue(status.is_client_error(response.status_code))
        self.login()
        response = self.client.get(url)
        self.assertTrue(status.is_success(response.status_code))

    def test_question_available_fields(self):
        available_fields = ('url', 'pk', 'has_answer', 'title', 'content', 'created_at')
        self.login()
        url = reverse('question-detail', kwargs={'pk': self.question.pk})
        response = self.client.get(url)
        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(sorted(available_fields), sorted(response.data.keys()))

    def test_answer_available_fields(self):
        available_fields = ('is_right', 'content', 'created_at')
        self.login()
        url = reverse('answers', kwargs={'q_pk': self.question.pk})
        response = self.client.get(url)
        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(sorted(available_fields), sorted(response.data['results'][0].keys()))

    def test_search(self):
        self.login()

        url = reverse('search', kwargs={'q': 'тело'})
        response = self.client.get(url)
        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(1, response.data['count'])
        self.assertEqual(self.question.pk, response.data['results'][0]['pk'])

        url = reverse('search', kwargs={'q': 'заголовок'})
        response = self.client.get(url)
        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(1, response.data['count'])
        self.assertEqual(self.question.pk, response.data['results'][0]['pk'])

        url = reverse('search', kwargs={'q': 'tag: Тег1'})
        response = self.client.get(url)
        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(1, response.data['count'])
        self.assertEqual(self.question.pk, response.data['results'][0]['pk'])

    def test_question_trending(self):
        self.login()

        url = reverse('question-trending')
        response = self.client.get(url)
        self.assertTrue(status.is_success(response.status_code))

    def test_question_paging(self):
        for i in range(20):
            Question.objects.create(author=self.user, title='Вопрос', content='Тело')
        self.login()

        url = reverse('question-list')
        response = self.client.get(url)
        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(22, response.data['count'])
        self.assertEqual(10, len(response.data['results']))

    def test_answers(self):
        for i in range(5):
            self.question2.answers.create(content='ответ', author=self.user)
        self.login()

        url = reverse('answers', kwargs={'q_pk': self.question2.pk})
        response = self.client.get(url)
        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(5, response.data['count'])
        self.assertEqual(5, len(response.data['results']))

    def test_answer_paging(self):
        for i in range(25):
            self.question2.answers.create(content='ответ', author=self.user)
        self.login()

        url = reverse('answers', kwargs={'q_pk': self.question2.pk})
        response = self.client.get(url)
        self.assertTrue(status.is_success(response.status_code))
        self.assertEqual(25, response.data['count'])
        self.assertEqual(10, len(response.data['results']))
