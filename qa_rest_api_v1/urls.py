from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt import views as jwt_views
from rest_framework_swagger.views import get_swagger_view
from qa_rest_api_v1.viewsets import QuestionViewSet, AnswerView, QuestionSearchView


router = routers.DefaultRouter()
router.register('question', QuestionViewSet, basename='question')

urlpatterns = [
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('search/<str:q>', QuestionSearchView.as_view(), name='search'),
    path('question/<int:q_pk>/answers/', AnswerView.as_view(), name='answers'),
    path('', get_swagger_view(title='Hasker API'), name='api_index'),
    path('', include(router.urls)),
]