from rest_framework import serializers
from qa_app.models import *


class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Question
        fields = ('url', 'pk', 'has_answer', 'title', 'content', 'created_at')


class AnswerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Answer
        fields = ('is_right', 'content', 'created_at')
