from rest_framework import viewsets
from rest_framework import mixins
from rest_framework import generics
from rest_framework import pagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from qa_rest_api_v1.serializers import *


class Size10Pagination(pagination.PageNumberPagination):
    page_size = 10


class QuestionViewSet(mixins.ListModelMixin,
                      mixins.RetrieveModelMixin,
                      viewsets.GenericViewSet):
    queryset = Question.get_last()
    serializer_class = QuestionSerializer
    pagination_class = Size10Pagination
    permission_classes = [IsAuthenticated]

    @action(detail=False, url_path='trending')
    def trending(self, request, *args, **kwargs):
        self.queryset = Question.get_trending()
        return self.list(request, *args, **kwargs)


class QuestionSearchView(generics.ListAPIView):
    serializer_class = QuestionSerializer
    pagination_class = Size10Pagination
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        if 'q' in self.kwargs:
            text = str(self.kwargs['q']).strip()
            if text.lower().startswith('tag:'):
                tag_name = text[4:].strip()
                try:
                    return Question.get_by_tag(tag_name)
                except ObjectDoesNotExist:
                    return Question.objects.none()
            else:
                return Question.search(text)


class AnswerView(generics.ListAPIView):
    serializer_class = AnswerSerializer
    pagination_class = Size10Pagination
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        if 'q_pk' in self.kwargs:
            try:
                return Question.objects.get(pk=self.kwargs['q_pk']).get_sorted_answers()
            except Question.DoesNotExist:
                pass
        return Answer.objects.none()
