from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from qa_app import views


urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('api/v1/', include('qa_rest_api_v1.urls')),
    path('user/', include('user_profile.urls'), name='user_profile'),
    path('qa/', include(('qa_app.urls', 'qa_app'), namespace='qa')),
    path('', views.IndexListView.as_view(), name='index'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
