from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.utils.safestring import mark_safe

from sorl.thumbnail import get_thumbnail

from user_profile.models import User


class RegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2', 'avatar']


class AdminImageWidget(forms.ClearableFileInput):
    def render(self, name, value, attrs=None, renderer=None):
        output = []
        if value and getattr(value, "url", None):
            t = get_thumbnail(value, '120x120')
            output.append('<img src="{}"/><br/>'.format(t.url))
        output.append(super().render(name, value, attrs, renderer))
        return mark_safe(u''.join(output))


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'avatar']
        widgets = {'username': forms.TextInput(attrs={'readonly': 'readonly'}), 'avatar': AdminImageWidget}
