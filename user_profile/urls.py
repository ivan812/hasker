from django.urls import path

from . import views

urlpatterns = [
    path('registration', views.UserCreationView.as_view(), name='registration'),
    path('registration_success', views.RegistrationSuccessView.as_view(), name='registration_success'),
    path('login', views.UserLoginView.as_view(), name='login'),
    path('logout', views.UserLogoutView.as_view(), name='logout'),
    path('accounts', views.UserProfileView.as_view(), name='account'),
    path('accounts/profile/', views.UserEditView.as_view(), name='profile'),
]