from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.admin import UserCreationForm
from sorl.thumbnail.admin import AdminImageMixin
from user_profile.models import User


class UserAdmin(AdminImageMixin, BaseUserAdmin):
    add_form = UserCreationForm
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'first_name', 'last_name', 'avatar', 'password1', 'password2')}
        ),
    )

    def __init__(self, *args, **kwargs):
        self.fieldsets[1][1]['fields'] += ('avatar',)
        super().__init__(*args, **kwargs)


# admin.site.unregister(User)
admin.site.register(User, UserAdmin)
