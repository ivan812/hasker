from django import views
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import TemplateView, CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import redirect, get_object_or_404
from user_profile.models import User
from user_profile.forms import RegistrationForm, UserForm


class RegistrationView(CreateView):
    model = User
    fields = ['username', 'password', 'first_name', 'last_name', 'email']
    template_name = 'registration.html'
    success_url = 'registration_success'


class RegistrationSuccessView(TemplateView):
    template_name = 'registration_success.html'


class UserCreationView(CreateView):
    form_class = RegistrationForm
    template_name = 'registration.html'
    success_url = 'registration_success'


class UserLoginView(LoginView):
    template_name = 'login.html'


class UserLogoutView(LogoutView):
    next_page = 'index'


class UserProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'account_menu.html'


class UserProfileEditView(LoginRequiredMixin, views.View):

    context_object_name = 'form_user'
    form_class = UserForm
    template_name = 'profile_form.html'
    login_url = 'login'

    def get(self, request, *args, **kwargs):
        form_user = UserForm(instance=request.user)
        return render(request, self.template_name, {'form_user': form_user})

    def post(self, request, *args, **kwargs):
        form_user = UserForm(instance=request.user, data=request.POST, files=request.FILES)
        if form_user.is_valid():
            form_user.save()
            return redirect('account')
        return render(request, self.template_name, {'form_user': form_user})


class UserEditView(LoginRequiredMixin, UpdateView):
    form_class = UserForm
    login_url = 'login'
    template_name = 'profile_form.html'

    def get_object(self):
        return get_object_or_404(User, pk=self.request.user.pk)

    def get_success_url(self):
        return reverse('account')
