from django.contrib.auth.models import AbstractUser
from sorl import thumbnail


class User(AbstractUser):
    avatar = thumbnail.ImageField(verbose_name='Avatar', upload_to='avatars', blank=False)
